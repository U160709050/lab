public class GCDLoop {

	public static void main(String[] args) {
		
		System.out.println(GCD(50,90));
		System.out.println(GCDRec(90,50));
		System.out.println(Int2Bin(199));
		

	}
	
	private static int GCD (int first, int second) {
		
		int result = 1 ;
		
		if (first < second) {
			
			int temp = first;
			first = second ;
			second = temp ;
					
		}
		for (int i = 1; i < second; i++) {
			
			if (first%i == 0 && second%i == 0) {
				
				result = i ;
				
			}
			
		}
		
		return result;
	}

	private static int GCDRec (int first, int second) {
		
		if (first < second) {
			
			int temp = first;
			first = second ;
			second = temp ;
					
		}
		
		
		if (second != 0) {
			
			return GCDRec(second , first%second);
			
		}
		else {
			return first;
		}
	}
	
	private static String Int2Bin (int integer) {
		
		String result = "" ;
		int i = 0;
		
		int[] array = new int[100] ;
		
		while (integer > 0) {
			array[i] = integer%2;
			i++;
			integer = integer/2;
		}
		for (int j = i ; j >= 0 ; j--) {
			result += array[j] ;
		}
		return result.substring(1, result.length());
	}
	
	
	
	}
