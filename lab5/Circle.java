package lab5;

public class Circle {
	
	int radius;

	public Circle(int r) {
		
		radius = r   ;
		
	}
	
	public double area() {
		
		return Math.PI*radius*radius  ;
	
    }
	
	public double perimeter() {
		
		return Math.PI*2*radius;
		
		
	}
	

}
