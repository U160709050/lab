package betul.shapes;

import java.lang.Math;

public class Circle extends Object {
	protected int radius;
	
	public Circle(int radius){
		super();
		this.radius=radius;
		System.out.println("Circle is being created");
	
	}

	public double area() {
		System.out.println("Circle is being created");
		return Math.PI *radius*radius ;
	}
}