package betul.main;

import java.util.ArrayList;

import betul.shapes3d.Cylinder;

public class Test3D {

	public static void main(String[] args) {
		ArrayList<Cylinder> cylinders = new ArrayList<>();
		Cylinder cylinder = new Cylinder(5);
		
		System.out.println(cylinder);
		
		cylinders.add(cylinder);
		cylinders.add(new Cylinder(6,7));
		System.out.println(cylinders);

	}

}
