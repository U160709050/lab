package drawingV2;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();
	
	
	public double calculateTotalArea() {
		double totalArea = 0;
		for (Object object : shapes) {
			if (object instanceof Circle) {
				Circle c1 = (Circle) object;
				totalArea += c1.area();
			}
			else if (object instanceof Rectangle) {
				Rectangle r1 = (Rectangle) object;
				totalArea += r1.area();
			}
			else if (object instanceof Square) {
				Square s1 = (Square) object;
				totalArea += s1.area();
			}
		}
		return totalArea;
	}
	
	public void addShape(Object object) {
		shapes.add(object);
	}
}
